#include <config.h>

#include <glibtop.h>
#include <glibtop/sysdeps.h>
#include <glibtop/union.h>

#if (defined HAVE_LIBINTL_H) || (defined HAVE_GETTEXT)
#include <libintl.h>
#else
#include <libgettext.h>
#endif

#ifndef _
#define _(String) dgettext (PACKAGE, String)
#define N_(String) (String)
#endif

#include <guile/gh.h>

SCM_GLOBAL_VCELL_INIT (s_synopsis_start_text, "synopsis-start-text", gh_str02scm (_("Automatically generated function synopsis:")));

SCM_GLOBAL_VCELL_INIT (s_description_start_text, "description-start-text", gh_str02scm (_("Automatically generated description:")));

SCM_GLOBAL_VCELL_INIT (s_definition_start_text, "definition-start-text", gh_str02scm (_("Automatically generated type definition:")));

SCM_GLOBAL_VCELL_INIT (s_parameter_start_text, "parameter-start-text", gh_str02scm (_("Automatically generated description of all structure entries:")));

void
boot_guile_gettext (void)
{
#include "guile-gettext.x"
};

void
main_prog (int argc, char *argv [])
{
	boot_guile_gettext ();

	gh_repl (argc, argv);
}

int
main (int argc, char *argv [])
{
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

	gh_enter (argc, argv, main_prog);

	exit (0);
}
