#!/home/martin/PERL/bin/perl -w

require 5.005;
use strict;

use Net::FTP;
use Carp;

die "Usage: $0 directory remote-path" unless $#ARGV == 1;

my $directory = $ARGV[0];

die "Invalid remote path (use `user\@host:path')" unless
  $ARGV[1] =~ /^(.*)\@(.*)\:(.*)$/;

my ($remote_user, $remote_host, $remote_dir) = ($1, $2, $3);

die "Uploading of $directory not allowed" unless -e "$directory/.upload.ok";

my $ftp = Net::FTP->new ($remote_host);

system "stty -echo";
print "Enter password for $remote_user\@$remote_host: ";
chop(my $password = <STDIN>);
print "\n";
system "stty echo";

$ftp->login ($remote_user, $password) or
  croak "Login failed: ".$ftp->message;

undef $password;

$ftp->mkdir ($remote_dir);

$ftp->cwd ($remote_dir) or croak "cwd ($remote_dir): ".$ftp->message;

sub upload_directory($$) {
  my ($source, $target) = @_;

  opendir DIR, $source or croak "opendir ($source): $!";

  foreach (readdir DIR) {
    if (/^\./) {
      next unless $_ eq '.htaccess';
    }

    if (-d qq[$source/$_]) {
      print "Entering directory $target/$_ ...\n";

      $ftp->mkdir (qq[$target/$_]);

      upload_directory (qq[$source/$_], qq[$target/$_]);
      print "Leaving directory $target/$_ ...\n";
      next;
    }

    print "Uploading $target/$_ ...\n";

    $ftp->delete (qq[$target/$_]);

    $ftp->put (qq[$source/$_], qq[$target/$_]) or
      croak "put ($target/$_): ".$ftp->message;
  }
  
  closedir DIR;
}

sub delete_remote_directory ($) {
  my ($remote) = @_;

  my @ls = $ftp->ls ($remote) or do {
    warn "ls ($remote): ".$ftp->message;
    return;
  };

  return unless $#ls;

  map { $_ = substr ($_, (length $remote)+1) } @ls if $ls [0] =~ m,^/,;

  foreach my $file (@ls) {
    my $dir = qq[$remote/$file/];
    my @list = $ftp->ls ($dir);

    if ($#list) {
      map { $_ = substr ($_, (length $dir)+1) } @list if $list [0] =~ m,^/,;

      print "Entering directory $remote/$file ...\n";
      delete_remote_directory (qq[$remote/$file]);
      print "Removing directory $remote/$file ...\n";
      $ftp->rmdir (qq[$remote/$file]) or croak "rmdir ($file): ".$ftp->message;
      print "Leaving directory $remote/$file ...\n";
    } else {
      print "Removing $remote/$file\n";
      $ftp->delete (qq[$remote/$file]) or croak "delete ($file): ".$ftp->message;
    }
  }
}

print STDERR "Removing remote directory $remote_dir ...\n";

delete_remote_directory ($remote_dir);

print STDERR "Uploading local directory $directory ...\n";

upload_directory ($directory, $remote_dir);
