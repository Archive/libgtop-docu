<!DOCTYPE style-sheet PUBLIC "-//James Clark//DTD DSSSL Style Sheet//EN" [
<!ENTITY % html "IGNORE">
<![%html;[
<!ENTITY % print "IGNORE">
<!ENTITY home-of-linux.dsl SYSTEM "home-of-linux.html.dsl" CDATA dsssl>
]]>
<!ENTITY % print "INCLUDE">
<![%print;[
<!ENTITY home-of-linux.dsl SYSTEM "home-of-linux.print.dsl" CDATA dsssl>
]]>
]>

<style-sheet>

<style-specification id="print" use="home-of-linux">
<style-specification-body> 

(define %default-language% "usen")

</style-specification-body>
</style-specification>

<style-specification id="html" use="home-of-linux">
<style-specification-body> 

(define %default-language% "usen")

</style-specification-body>
</style-specification>

<external-specification id="home-of-linux" document="home-of-linux.dsl">

</style-sheet>
